<?
	$script =  basename($_SERVER['SCRIPT_NAME'], ".php"); // Получаем имя скрипта, с которого запустили (должно соответствовать файлу шаблона)

	$generator = new html_generator();
	$generator -> load_template("tpl/$script.html");
	$generator -> mount_vars($configs);
	$generator -> if_compiler();
	$generator -> foreach_compiler();
	$generator->print_out();
?>