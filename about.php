﻿<?php
/**
 * Created by PhpStorm.
 * Author: Konstantin Fedorov
 * Date: 11.04.2017
 */

// переменные
$configs['title'] = 'О скрипте';
$configs['text'] = 'Простой скрипт шаблонизатора, этот текст выведен из переменной, объявленной в about.php, так же данные можно выводить из базы данных.';

// подгружаем генератор
require_once 'lib/TemplateEngine.php';
require_once 'controller/TplGen.php';

?>