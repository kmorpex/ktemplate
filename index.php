﻿<?php
/**
 * Created by PhpStorm.
 * Author: Fedorov Konstantin
 * Date: 11.04.2017
 */
	
// переменные
$configs['title'] = 'Ktmpl';
$configs['text'] = 'Простой шаблонизатор, предназначенный для удобства.';

// подгружаем генератор
require_once 'lib/TemplateEngine.php';
require_once 'controller/TplGen.php';

?>